# App_Conversor_de_Moedas
<h2>Sobre o Projeto</h2>
<p>Coversor de moedas simples e atraente. 😊</p>
<h3>Front-end:</h3>
<ul>
  <li>JavaScript</li>
</ul>
<h3>Back-end:</h3>
<ul>
  <li>Database: Firebase</li>
</ul>
<h3>Stacks:</h3>
<ul>
  <li>React-native</li>
</ul>

![appConversorDeMoedalSmallGif](https://user-images.githubusercontent.com/82960240/138720047-3ba4a323-6756-44be-b1f9-020773029e55.gif)

<hr />
<h3>Autor</h3>
<h4>Raissa Arcaro Daros</h4>
<div style="display: inline_block;"><br>
   
[![Blog](https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white)](https://www.instagram.com/raissa_dev/)
[![Blog](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/raissa-dev-69986a214/)
[![Blog](https://img.shields.io/badge/GitHub-100000?style=for-the-badge&logo=github&logoColor=white)](https://github.com/Raissadev/)  
   
</div>
